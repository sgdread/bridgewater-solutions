package com.org.blob;

import java.util.Objects;

public class Solution {

    protected int cellReads;
    protected int top;
    protected int left;
    protected int bottom;
    protected int right;

    public Solution(int cellReads, int top, int left, int bottom, int right) {
        this.cellReads = cellReads;
        this.top = top;
        this.left = left;
        this.bottom = bottom;
        this.right = right;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Cell Reads:").append(cellReads);
        sb.append("\nTop:").append(top);
        sb.append("\nLeft:").append(left);
        sb.append("\nBottom:").append(bottom);
        sb.append("\nRight:").append(right);
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Solution solution = (Solution) o;
        return top == solution.top && left == solution.left && bottom == solution.bottom && right == solution.right;
    }

    @Override
    public int hashCode() {
        return Objects.hash(top, left, bottom, right);
    }

}
