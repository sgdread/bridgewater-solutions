package com.org.blob;

import java.util.Objects;

/**
 * Tuple to store row-column pair
 */
public class Tuple {

    private final int row;
    private final int col;

    public Tuple(int row, int col) {
        this.row = row;
        this.col = col;
    }

    /**
     * Constructs Tuple
     *
     * @param row row
     * @param col column
     * @return Tuple
     */
    public static Tuple of(int row, int col) {
        return new Tuple(row, col);
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public Tuple moveUp() {
        return Tuple.of(row - 1, col);
    }

    public Tuple moveDown() {
        return Tuple.of(row + 1, col);
    }

    public Tuple moveLeft() {
        return Tuple.of(row, col - 1);
    }

    public Tuple moveRight() {
        return Tuple.of(row, col + 1);
    }

    @Override
    public String toString() {
        return "{" + row + ", " + col + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tuple tuple = (Tuple) o;
        return row == tuple.row &&
                col == tuple.col;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col);
    }
}
