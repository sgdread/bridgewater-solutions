package com.org.blob;

/**
 * This is a naive brute-force approach, it just scans top constraint, then bottom, then left then right (see pdf)
 * <p>
 * Pros: performs great on blobs with large perimeter with almost no margin (1-2 rows)
 * <p>
 * Deficiencies: This solver is performs bad on small blobs located in the center of solution space. Worst case
 * is 2-cell blob in the center of grid.
 */
public class Solver1 {

    private static final int SIZE = 10;

    private Blob blob;

    private Solver1() {
        // Do nothing
    }

    /**
     * Constructs solver using the provided blob
     *
     * @param blob problem definition
     * @return solver
     */
    public static Solver1 withBlob(Blob blob) {
        Solver1 solver = new Solver1();
        solver.blob = blob;
        return solver;
    }


    /**
     * @return blob boundaries.
     */
    public Solution solve() {
        Tuple top = findTopConstraint();
        if (top == null) throw new IllegalArgumentException("No blob found - empty search area");
        Tuple bottom = findBottomConstraint(top);
        if (top.equals(bottom)) throw new IllegalArgumentException("Not a blob");

        int left = findLeftConstrain(top, bottom);
        int right = findRightConstrain(top, bottom);
        return new Solution(blob.getReads(), top.getRow(), left, bottom.getRow(), right);
    }

    private int findRightConstrain(Tuple top, Tuple bottom) {
        for (int row = top.getRow() + 1; row < bottom.getRow(); row++) {
            for (int col = SIZE - 1; col > bottom.getCol(); col--) {
                if (blob.get(col, row)) return col;
            }
        }
        return bottom.getCol();
    }

    private int findLeftConstrain(Tuple top, Tuple bottom) {
        for (int row = top.getRow() + 1; row < bottom.getRow(); row++) {
            for (int col = 0; col < top.getCol(); col++) {
                if (blob.get(col, row)) return col;
            }
        }
        return top.getCol();
    }

    private Tuple findBottomConstraint(Tuple top) {
        for (int row = SIZE - 1; row >= top.getRow(); row--) {
            for (int col = SIZE - 1; col >= 0; col--) {
                if (blob.get(col, row)) {
                    return (col == top.getCol() && row == top.getRow()) ? top : new Tuple(row, col);
                }
            }
        }

        return null; // will never happen
    }

    private Tuple findTopConstraint() {
        for (int row = 0; row < SIZE; row++) {
            for (int col = 0; col < SIZE; col++) {
                if (blob.get(col, row)) return new Tuple(row, col);
            }
        }
        return null;
    }
}
