package com.org.blob;

import com.org.blob.walker.Solver3;

public class Runner {

    public static void main(String[] args) {
        // @formatter:off
        String[] data = {
                 "0000000000",
                 "0011100000",
                 "0011111000",
                 "0010001000",
                 "0011111000",
                 "0000101000",
                 "0000101000",
                 "0000111000",
                 "0000000000",
                 "0000000000"
         };
        // @formatter:on

        System.out.println("-----  Solver 1");
        System.out.println(Solver1.withBlob(Blob.fromData(data)).solve());

        System.out.println("-----  Solver 2");
        System.out.println(Solver2.withBlob(Blob.fromData(data)).solve());

        System.out.println("-----  Solver 3");
        System.out.println(Solver3.withBlob(Blob.fromData(data)).solve());
    }

}
