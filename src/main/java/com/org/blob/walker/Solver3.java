package com.org.blob.walker;

import com.org.blob.Blob;
import com.org.blob.Solution;
import com.org.blob.Tuple;

/**
 * This implementation using following approach:
 * <ul>
 * <li>find first cell in the blob (using modified "checkers" approach: scanning two rows at the same time),</li>
 * <li>do walk-around clockwise using rules based on patterns in 4x4 cells near current walk positions (see pdf)</li>
 * </ul>
 * Existing solution can be optimized even further - see page 2 in pdf (haven't been implemented).
 * <p>
 * Pros: Faster times for majority cases
 * <p>
 * Deficiencies:
 * <ul>
 * <li>worst case scenarios are "maze"-like blobs covering the whole solution space (i.e. the ones had long perimeter)</li>
 * <li>another worst-case scenario for the current implementation is smal blob in right-bottom corner - this is due to
 * chosen implementation of first cell finder. Alternative approach can be randomized point selection, but this will
 * need additional research and coding effort on how to find external boundary of the blob (escaping from internal
 * empty "pools")</li>
 * </ul>
 */
public class Solver3 {

    private static final int SIZE = 10;

    // bits  3 2
    //       1 0
    private int state;
    private Tuple initialPosition;
    private Tuple position;
    private Walker.Move lastMove = Walker.Move.UNKNOWN;

    private Blob blob;

    private Solver3() {
        // Do nothing
    }

    /**
     * Constructs solver using the provided blob
     *
     * @param blob problem definition
     * @return solver
     */
    public static Solver3 withBlob(Blob blob) {
        Solver3 solver = new Solver3();
        solver.blob = blob;
        return solver;
    }

    /**
     * @return blob boundaries.
     */
    public Solution solve() {
        // find position to start
        initialPosition = getInitialPosition();
        position = initialPosition;
        state = Walker.getState(new boolean[]{
                blob.get(position.getCol() - 1, position.getRow() - 1),
                blob.get(position.getCol(), position.getRow() - 1),
                blob.get(position.getCol() - 1, position.getRow()),
                blob.get(position.getCol(), position.getRow())
        });

        WalkerSolution solution = new WalkerSolution(position);

        // travers loop around blob
        do { // until we returned to initial position
            Walker.nextState(blob, position, lastMove, state, (moveTo, newState, newPosition) -> {
                if (initialPosition == null && state > 0 && state < 15) {
                    initialPosition = position;
                }
                lastMove = moveTo;
                state = newState;
                position = newPosition;
                solution.handleMove(moveTo);
            });
        } while (!position.equals(initialPosition));

        solution.setCellReads(blob.getReads());

        return solution;
    }

    private Tuple getInitialPosition() {
        for (int row = 0; row < SIZE; row += 2) {
            for (int col = 0; col < SIZE; col++) {
                int row_ = (col % 2 == 0) ? row : row + 1;
                if (blob.get(col, row_)) return new Tuple(row_, col);
            }
        }
        throw new IllegalArgumentException("Not a blob - empty space");
    }

}
