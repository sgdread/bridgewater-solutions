package com.org.blob.walker;

import com.org.blob.Blob;
import com.org.blob.Tuple;

import static com.org.blob.walker.Walker.Move.*;

/**
 * Tooling for blob walking
 */
public class Walker {

    public enum Move {
        UNKNOWN, UP, DOWN, LEFT, RIGHT
    }

    public static void nextState(Blob blob, Tuple currentPosition, Move lastMove, int currentState, MoveHandler onMoveCallback) {
        Move moveTo = nextMove(lastMove, currentState);
        int newState;
        Tuple newPosition;
        switch (moveTo) {
            case UP:
                newState = moveUp(blob, currentPosition, currentState);
                newPosition = currentPosition.moveUp();
                break;
            case DOWN:
                newState = moveDown(blob, currentPosition, currentState);
                newPosition = currentPosition.moveDown();
                break;
            case LEFT:
                newState = moveLeft(blob, currentPosition, currentState);
                newPosition = currentPosition.moveLeft();
                break;
            case RIGHT:
                newState = moveRight(blob, currentPosition, currentState);
                newPosition = currentPosition.moveRight();
                break;
            default:
                throw new IllegalStateException("Unable to handle state - illegal move");
        }
        onMoveCallback.onMove(moveTo, newState, newPosition);
    }

    public static int moveUp(Blob blob, Tuple currentPosition, int currentState) {
        int newState = currentState >> 2;
        newState += (asInt(blob.get(currentPosition.getCol() - 1, currentPosition.getRow() - 2)) << 3);
        newState += (asInt(blob.get(currentPosition.getCol(), currentPosition.getRow() - 2)) << 2);
        return newState;
    }

    public static int moveDown(Blob blob, Tuple currentPosition, int currentState) {
        int newState = (currentState << 2) & 0b1111; // only use lower 4 bits
        newState += (asInt(blob.get(currentPosition.getCol() - 1, currentPosition.getRow() + 1)) << 1);
        newState += asInt(blob.get(currentPosition.getCol(), currentPosition.getRow() + 1));
        return newState;
    }

    public static int moveLeft(Blob blob, Tuple currentPosition, int currentState) {
        int newState = (currentState >> 1) & 0b0101; // keep only bits 0 and 2 (right side)
        newState += (asInt(blob.get(currentPosition.getCol() - 2, currentPosition.getRow() - 1)) << 3);
        newState += (asInt(blob.get(currentPosition.getCol() - 2, currentPosition.getRow())) << 1);
        return newState;
    }

    public static int moveRight(Blob blob, Tuple currentPosition, int currentState) {
        int newState = (currentState << 1) & 0b1010; // keep only bits 1 and 3 (right side)
        newState += (asInt(blob.get(currentPosition.getCol() + 1, currentPosition.getRow() - 1)) << 2);
        newState += asInt(blob.get(currentPosition.getCol() + 1, currentPosition.getRow()));
        return newState;
    }

    private static int asInt(boolean b) {
        return b ? 1 : 0;
    }

    public static Move nextMove(Move lastMove, int state) {
        switch (state) {
            case 0:
                return RIGHT;
            case 1:
                return RIGHT;
            case 2:
                return DOWN;
            case 3:
                return RIGHT;
            case 4:
                return UP;
            case 5:
                return UP;
            case 6:
                return lastMove == LEFT ? DOWN : UP;
            case 7:
                return UP;
            case 8:
                return LEFT;
            case 9:
                return lastMove == UP ? LEFT : RIGHT;
            case 10:
                return DOWN;
            case 11:
                return RIGHT;
            case 12:
                return LEFT;
            case 13:
                return LEFT;
            case 14:
                return DOWN;
            case 15:
                return LEFT;
            default:
                return UNKNOWN; // including cases 0 and 15
        }
    }

    public static int getState(boolean[] cells) {
        int result = 0;
        int size = cells.length;
        for (int i = 0; i < size; i++) {
            result += (cells[i] ? 1 : 0) << (size - i - 1);
        }
        return result;
    }

    protected interface MoveHandler {
        void onMove(Move move, int newState, Tuple newPosition);
    }


}
