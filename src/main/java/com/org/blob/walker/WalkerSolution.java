package com.org.blob.walker;

import com.org.blob.Solution;
import com.org.blob.Tuple;

/**
 * Solution wrapper which does automatic tracking of expanding boundaries as we walk around blob.
 */
public class WalkerSolution extends Solution {

    private int currentLeftDelta = 0;
    private int currentRightDelta = 0;
    private int currentTopDelta = 0;
    private int currentDownDelta = 0;
    private int maxTopDelta = 0;
    private int maxLeftDelta = 0;
    private int maxRightDelta = 1;
    private int maxDownDelta = 1;

    public WalkerSolution(Tuple start) {
        super(0, start.getRow(), start.getCol(), start.getRow(), start.getCol());
    }

    /**
     * Account walk to track boundaries.
     *
     * @param move move to account
     */
    public void handleMove(Walker.Move move) {
        switch (move) {
            case UP:
                currentTopDelta++;
                currentDownDelta--;
                if (maxTopDelta < currentTopDelta) {
                    maxTopDelta++;
                    top--;
                }
                break;
            case DOWN:
                currentDownDelta++;
                currentTopDelta--;
                if (maxDownDelta < currentDownDelta) {
                    maxDownDelta++;
                    bottom++;
                }
                break;
            case LEFT:
                currentLeftDelta++;
                currentRightDelta--;
                if (maxLeftDelta < currentLeftDelta) {
                    maxLeftDelta++;
                    left--;
                }

                break;
            case RIGHT:
                currentLeftDelta--;
                currentRightDelta++;
                if (maxRightDelta < currentRightDelta) {
                    maxRightDelta++;
                    right++;
                }
                break;
        }
    }

    public void setCellReads(int cellReads) {
        this.cellReads = cellReads;
    }

    public Solution toSolution() {
        return new Solution(cellReads, top, left, bottom, right);
    }
}
