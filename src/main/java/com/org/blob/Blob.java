package com.org.blob;

import com.google.common.collect.Sets;

import java.util.Set;

/**
 * Wrapper class which simplifies working with blob
 */
public class Blob {

    private boolean[][] data;
    private int cellReads = 0;

    private final Set<Tuple> knownCellReads = Sets.newHashSet();

    private Blob() {
        // do nothing
    }

    /**
     * Retrieves cell at [row, col] in underlying boolean array.
     * Also does reads count for first-time cell reads.
     *
     * @param col column
     * @param row row
     * @return state of requested cell.
     */
    public boolean get(int col, int row) {
        // out of bounds - need a value for walker, should not be counted as read, but here's a good place to do this check for uniform calls
        if (col < 0 || col > data.length || row < 0 || row > data.length) return false;

        // We do additional known cell reads in algorithms - this can be eliminated by just caching; so we won't count those reads
        Tuple cell = Tuple.of(row, col);
        if (!knownCellReads.contains(cell)) {
            cellReads++;
            knownCellReads.add(cell);
        }
        return data[row][col];
    }

    /**
     * Number of reads current data structure experienced (only unique reads)
     * @return
     */
    public int getReads() {
        return cellReads;
    }

    public static Blob fromData(String[] data) {
        if (data == null) throw new IllegalArgumentException("Wrong format");
        Blob blob = new Blob();
        boolean[][] result = new boolean[data.length][];
        for (int i = 0; i < data.length; i++) {
            String rowStr = data[i];
            boolean[] row = new boolean[rowStr.length()];
            for (int j = 0; j < row.length; j++) {
                row[j] = rowStr.charAt(j) == '1';
            }
            result[i] = row;
        }
        blob.data = result;
        return blob;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (boolean[] aData : data) {
            if (sb.length() > 0) sb.append("\n");
            for (boolean aRow : aData) {
                sb.append(aRow ? '1' : '0');
            }
        }
        return sb.toString();
    }
}
