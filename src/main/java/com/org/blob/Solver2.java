package com.org.blob;

/**
 * This implementation is using optimization based on fact that blob always at least one adjacent cell populated.
 * This means we can hop cells on check and utilize "checkers" scanning pattern which makes this approach potentially
 * up to 2x faster compared to Solution 1. One note: in case if we found populated cell, we have to check previous row
 * for missed entries (see arrows on the picture in pdf) - this will address J-pattern test scenarios in test
 * pack for the solvers (com.org.blob.AbstractSolverTest class).
 * <p>
 * Pros: Same as Solver 1
 * <p>
 * Deficiencies: This solver has the same problems as Solver 1, it's just ~2x faster in worst case scenario.
 */
public class Solver2 {

    private static final int SIZE = 10;

    private Blob blob;

    private Solver2() {
        // Do nothing
    }

    /**
     * Constructs solver using the provided blob
     *
     * @param blob problem definition
     * @return solver
     */
    public static Solver2 withBlob(Blob blob) {
        Solver2 solver = new Solver2();
        solver.blob = blob;
        return solver;
    }


    /**
     * @return blob boundaries.
     */
    public Solution solve() {
        int top = findTopConstraint();
        int bottom = findBottomConstraint();
        int left = findLeftConstraint(top, bottom);
        int right = findRightConstraint(top, bottom);
        return new Solution(blob.getReads(), top, left, bottom, right);
    }

    private int findRightConstraint(int top, int bottom) {
        boolean found = false;
        int boundary = SIZE - 1;
        int result = boundary;
        for (int col = boundary; col >= 0; col--) {
            for (int row = even(col + top) ? top : top + 1; row <= bottom; row += 2) {
                if (blob.get(col, row)) {
                    result = col;
                    if (col == boundary) return result;
                    if (!found) {
                        col++;
                        if (blob.get(col, row)) result = col;
                        found = true;
                    } else return result;
                }
            }
            if (found) break;
        }
        return result;
    }

    private int findLeftConstraint(int top, int bottom) {
        boolean found = false;
        int boundary = 0;
        int result = boundary;
        for (int col = boundary; col < SIZE; col++) {
            for (int row = even(col + top) ? top : top + 1; row <= bottom; row += 2) {
                if (blob.get(col, row)) {
                    result = col;
                    if (col == boundary) return result;
                    if (!found) {
                        col--;
                        if (blob.get(col, row)) result = col;
                        found = true;
                    } else return result;
                }
            }
            if (found) break;
        }
        return result;
    }

    private int findTopConstraint() {
        boolean found = false;
        int boundary = 0;
        int result = boundary;
        for (int row = boundary; row < SIZE; row++) {
            for (int col = even(row) ? 0 : 1; col < SIZE; col += 2) {
                if (blob.get(col, row)) {
                    result = row;
                    if (row == boundary) return result;
                    if (!found) {
                        row--;
                        if (blob.get(col, row)) result = row;
                        found = true;
                    } else return result;

                }
            }
            if (found) break;
        }
        return result;
    }

    private int findBottomConstraint() {
        boolean found = false;
        int boundary = SIZE - 1;
        int result = boundary;
        for (int row = boundary; row >= 0; row--) {
            for (int col = even(row) ? 0 : 1; col < SIZE; col += 2) {
                if (blob.get(col, row)) {
                    result = row;
                    if (row == boundary) return result;
                    if (!found) {
                        row++;
                        if (blob.get(col, row)) result = row;
                        found = true;
                    } else return result;
                }
            }
            if (found) break;
        }
        return result;
    }

    private static boolean even(int i) {
        return i % 2 == 0;
    }
}
