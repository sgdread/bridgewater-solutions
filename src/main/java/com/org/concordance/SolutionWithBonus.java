package com.org.concordance;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Map.Entry.comparingByKey;
import static java.util.stream.Collectors.*;

/**
 * Concordance solution (with bonus)
 */
public class SolutionWithBonus {

    private static final String WORD_WITH_PUNCTUATION_AT_THE_END_REGEX = "\\w+[.!?]+";

    public static void main(String[] args) {

        // Normally this would be prepared as a resource for parser
        Set<String> knownAbbreviations = Sets.newHashSet("i.e.");

        String input =
                "Given an arbitrary text document written in English, write a program that will generate a\n"
                        + "concordance, i.e. an alphabetical list of all word occurrences, labeled with word frequencies.\n"
                        + "Bonus: label each word with the sentence numbers in which each occurrence appeared.";

        AtomicInteger sentence = new AtomicInteger(1);

        Map<String, List<Integer>> wordToSentence =
                Arrays.stream(input.split("\\s+")) // split on whitespace
                      .map(word -> word.trim().toLowerCase())  // normalize words
                      .map(word -> { // map word to pair of wor and sentence number
                          if (knownAbbreviations.contains(word)) {
                              return Pair.of(word, sentence.get());
                          }
                          Pair<String, Integer> result = Pair.of(word.replaceAll("\\W+", ""), sentence.get());
                          if (word.matches(WORD_WITH_PUNCTUATION_AT_THE_END_REGEX)) {
                              // next sentence - increment sentence counter
                              sentence.incrementAndGet();
                          }
                          return result;
                      })
                      // Group Pairs by word and aggregate all sentence numbers into a single list
                      .collect(groupingBy(Pair::getKey, mapping(Pair::getValue, toList())));

        //Sort a map and aggregate stats
        final Map<String, String> sortedWordToSentence = new LinkedHashMap<>();
        final AtomicInteger longestWord = new AtomicInteger();
        wordToSentence.entrySet().stream()
                      .sorted(comparingByKey())
                      .forEachOrdered(e -> {
                          // Look for longest word - this will help with printing later
                          if (longestWord.get() < e.getKey().length()) longestWord.set(e.getKey().length());
                          // build {3:1,2,3} part of output from aggregations
                          String frequencyStats = "{" + e.getValue().size() + ":" + Joiner.on(',').join(e.getValue()) + "}";
                          sortedWordToSentence.put(e.getKey(), frequencyStats);
                      });

        // Printing
        printResult(sortedWordToSentence, longestWord);

    }

    private static void printResult(Map<String, String> sortedWordToSentence, AtomicInteger longestWord) {
        int c = 0;
        for (String key : sortedWordToSentence.keySet()) {
            String prefixLetter = String.valueOf((char) ('a' + (c % 26)));
            String prefix = Strings.repeat(prefixLetter, c / 26 + 1);

            String line = Joiner.on(' ').join(
                    Lists.newArrayList(
                            Strings.padEnd(prefix + ".", sortedWordToSentence.size() / 26 + 2, ' '),
                            Strings.padEnd(key, longestWord.get(), ' '),
                            sortedWordToSentence.get(key)));
            c++;
            System.out.println(line);
        }
    }
}
