package com.org.concordance;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

/**
 * Concordance solution (without bonus)
 */
public class Solution {

    public static void main(String[] args) {
        Set<String> knownAbbreviations = Sets.newHashSet("i.e.");

        String input = "Given an arbitrary text document written in English, write a program that will generate a\n"
                + "concordance, i.e. an alphabetical list of all word occurrences, labeled with word frequencies.\n"
                + "Bonus: label each word with the sentence numbers in which each occurrence appeared.";

        // Split,normalization
        Map<String, Long> wordToCount = Arrays.stream(input.split("\\s+")) // split on whitespace
                                              .map(word -> word.trim().toLowerCase())
                                              .map(word -> knownAbbreviations.contains(word) ? word : word.replaceAll("\\W", "")) // remove non-word chars
                                              .collect(groupingBy(identity(), counting()));

        final Map<String, Long> sorted = new LinkedHashMap<>();
        final AtomicInteger longestWord = new AtomicInteger();

        //Sort a map
        wordToCount.entrySet().stream()
                   .sorted(Map.Entry.comparingByKey())
                   .forEachOrdered(e -> {
                       if (longestWord.get() < e.getKey().length()) longestWord.set(e.getKey().length());
                       sorted.put(e.getKey(), e.getValue());
                   });

        // Printing
        int c = 0;
        for (String key : sorted.keySet()) {
            String prefixLetter = String.valueOf((char) ('a' + (c % 26)));
            String prefix = Strings.repeat(prefixLetter, c / 26 + 1);

            String line = Joiner.on(' ')
                                .join(Lists.newArrayList(
                                        Strings.padEnd(prefix + ".", sorted.size() / 26 + 2, ' '),
                                        Strings.padEnd(key, longestWord.get(), ' '),
                                        "{" + sorted.get(key) + "}"
                                ));


            c++;
            System.out.println(line);
        }

    }
}
