package com.org.blob;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Blob solver test suite - applied to all solvers.
 */
public abstract class AbstractSolverTest {

    protected abstract Solution solveUsingData(String[] data);

    @Test
    public void solver_should_correctly_solve_assignment() throws Exception {
        // @formatter:off
        String[] data = {
                 "0000000000",
                 "0011100000",
                 "0011111000",
                 "0010001000",
                 "0011111000",
                 "0000101000",
                 "0000101000",
                 "0000111000",
                 "0000000000",
                 "0000000000"
         };
        // @formatter:on

        assertEquals(new Solution(-1, 1, 2, 7, 6), solveUsingData(data));
    }

    @Test
    public void solver_should_handle_single_horizontal_line() throws Exception {
        // @formatter:off
        String[] data = {
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000110000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000"
         };
        // @formatter:on

        assertEquals(new Solution(-1, 4, 4, 4, 5), solveUsingData(data));
    }

    @Test
    public void solver_should_handle_cross() throws Exception {
        // @formatter:off
        String[] data = {
                 "0100000000",
                 "1110000000",
                 "0100000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000"
         };
        // @formatter:on

        assertEquals(new Solution(-1, 0, 0, 2, 2), solveUsingData(data));
    }


    @Test
    public void solver_should_handle_cross_shifted_down() throws Exception {
        // @formatter:off
        String[] data = {
                 "0000000000",
                 "0100000000",
                 "1110000000",
                 "0100000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000"
         };
        // @formatter:on

        assertEquals(new Solution(-1, 1, 0, 3, 2), solveUsingData(data));
    }

    @Test
    public void solver_should_handle_cross_shifted_right() throws Exception {
        // @formatter:off
        String[] data = {
                 "0010000000",
                 "0111000000",
                 "0010000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000"
         };
        // @formatter:on

        assertEquals(new Solution(-1, 0, 1, 2, 3), solveUsingData(data));
    }

    @Test
    public void solver_should_handle_j_shape() throws Exception {
        // @formatter:off
        String[] data = {
                 "0001000000",
                 "0101000000",
                 "0111000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000"
         };
        // @formatter:on

        assertEquals(new Solution(-1, 0, 1, 2, 3), solveUsingData(data));
        System.out.println(solveUsingData(data));
    }

    @Test
    public void solver_should_handle_j_shape_shifted_right() throws Exception {
        // @formatter:off
        String[] data = {
                 "0000100000",
                 "0010100000",
                 "0011100000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000"
         };
        // @formatter:on

        assertEquals(new Solution(-1, 0, 2, 2, 4), solveUsingData(data));
    }

    @Test
    public void solver_should_handle_j_shape_shifted_down() throws Exception {
        // @formatter:off
        String[] data = {
                 "0000000000",
                 "0001000000",
                 "0101000000",
                 "0111000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000"
         };
        // @formatter:on

        assertEquals(new Solution(-1, 1, 1, 3, 3), solveUsingData(data));
    }

    @Test
    public void solver_should_handle_single_vertical_line() throws Exception {
        // @formatter:off
        String[] data = {
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000100000",
                 "0000100000",
                 "0000000000",
                 "0000000000",
                 "0000000000",
                 "0000000000"
         };
        // @formatter:on

        assertEquals(new Solution(-1, 4, 4, 5, 4), solveUsingData(data));
    }

}