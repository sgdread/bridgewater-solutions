package com.org.blob;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TupleTest {

    @Test
    public void testMoveUp() throws Exception {
        assertEquals(Tuple.of(4, 5), Tuple.of(5,5).moveUp());
    }

    @Test
    public void testMoveDown() throws Exception {
        assertEquals(Tuple.of(6, 5), Tuple.of(5,5).moveDown());
    }

    @Test
    public void testMoveLeft() throws Exception {
        assertEquals(Tuple.of(5, 4), Tuple.of(5,5).moveLeft());
    }

    @Test
    public void testMoveRight() throws Exception {
        assertEquals(Tuple.of(5, 6), Tuple.of(5,5).moveRight());
    }
}