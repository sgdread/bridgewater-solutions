package com.org.blob;

import com.org.blob.walker.Walker;
import org.junit.Test;

import static com.org.blob.walker.Walker.Move.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class WalkerTest {
    @Test
    public void getState() throws Exception {
        int i = 0;
        assertEquals(i++, Walker.getState(new boolean[] {false, false, false, false}));
        assertEquals(i++, Walker.getState(new boolean[] {false, false, false, true}));
        assertEquals(i++, Walker.getState(new boolean[] {false, false, true, false}));
        assertEquals(i++, Walker.getState(new boolean[] {false, false, true, true}));
        assertEquals(i++, Walker.getState(new boolean[] {false, true, false, false}));
        assertEquals(i++, Walker.getState(new boolean[] {false, true, false, true}));
        assertEquals(i++, Walker.getState(new boolean[] {false, true, true, false}));
        assertEquals(i++, Walker.getState(new boolean[] {false, true, true, true}));
        assertEquals(i++, Walker.getState(new boolean[] {true, false, false, false}));
        assertEquals(i++, Walker.getState(new boolean[] {true, false, false, true}));
        assertEquals(i++, Walker.getState(new boolean[] {true, false, true, false}));
        assertEquals(i++, Walker.getState(new boolean[] {true, false, true, true}));
        assertEquals(i++, Walker.getState(new boolean[] {true, true, false, false}));
        assertEquals(i++, Walker.getState(new boolean[] {true, true, false, true}));
        assertEquals(i++, Walker.getState(new boolean[] {true, true, true, false}));
        assertEquals(i++, Walker.getState(new boolean[] {true, true, true, true}));
    }

    @Test
    public void move_regular_cases() throws Exception {
        // @formatter:off
        assertEquals(UNKNOWN, Walker.nextMove(UNKNOWN, -1)); // Normally not possible
        assertEquals(RIGHT,  Walker.nextMove(UNKNOWN, 0));
        assertEquals(RIGHT,   Walker.nextMove(UNKNOWN, 1));
        assertEquals(DOWN,    Walker.nextMove(UNKNOWN, 2));
        assertEquals(RIGHT,   Walker.nextMove(UNKNOWN, 3));
        assertEquals(UP,      Walker.nextMove(UNKNOWN, 4));
        assertEquals(UP,      Walker.nextMove(UNKNOWN, 5));
        assertEquals(UP,      Walker.nextMove(RIGHT, 6));
        assertEquals(DOWN,    Walker.nextMove(LEFT, 6));
        assertEquals(UP,      Walker.nextMove(UNKNOWN, 7));
        assertEquals(LEFT,    Walker.nextMove(UNKNOWN, 8));
        assertEquals(RIGHT,   Walker.nextMove(DOWN, 9));
        assertEquals(LEFT,    Walker.nextMove(UP, 9));
        assertEquals(DOWN,    Walker.nextMove(UNKNOWN, 10));
        assertEquals(RIGHT,   Walker.nextMove(UNKNOWN, 11));
        assertEquals(LEFT,    Walker.nextMove(UNKNOWN, 12));
        assertEquals(LEFT,    Walker.nextMove(UNKNOWN, 13));
        assertEquals(DOWN,    Walker.nextMove(UNKNOWN, 14));
        assertEquals(LEFT,    Walker.nextMove(UNKNOWN, 15));
        assertEquals(UNKNOWN, Walker.nextMove(UNKNOWN, 16)); // Normally not possible
        // @formatter:on
    }

    @Test
    public void moveUp() throws Exception {
        moveUpVerification(false, false, 6, 1);
        moveUpVerification(false, true, 6, 5);
        moveUpVerification(true, false, 6, 9);
        moveUpVerification(true, true, 6, 13);
        moveUpVerification(false, false, 9, 2);
        moveUpVerification(false, true, 9, 6);
        moveUpVerification(true, false, 9, 10);
        moveUpVerification(true, true, 9, 14);
    }

    @Test
    public void moveDown() throws Exception {
        moveDownVerification(false, false, 6, 8);
        moveDownVerification(false, true, 6, 9);
        moveDownVerification(true, false, 6, 10);
        moveDownVerification(true, true, 6, 11);
        moveDownVerification(false, false, 9, 4);
        moveDownVerification(false, true, 9, 5);
        moveDownVerification(true, false, 9, 6);
        moveDownVerification(true, true, 9, 7);
    }

    @Test
    public void moveLeft() throws Exception {
        moveLeftVerification(false, false, 6, 1);
        moveLeftVerification(false, true, 6, 3);
        moveLeftVerification(true, false, 6, 9);
        moveLeftVerification(true, true, 6, 11);
        moveLeftVerification(false, false, 9, 4);
        moveLeftVerification(false, true, 9, 6);
        moveLeftVerification(true, false, 9, 12);
        moveLeftVerification(true, true, 9, 14);
    }

    @Test
    public void moveRight() throws Exception {
        moveRightVerification(false, false, 6, 8);
        moveRightVerification(false, true, 6, 9);
        moveRightVerification(true, false, 6, 12);
        moveRightVerification(true, true, 6, 13);
        moveRightVerification(false, false, 9, 2);
        moveRightVerification(false, true, 9, 3);
        moveRightVerification(true, false, 9, 6);
        moveRightVerification(true, true, 9, 7);
    }

    private void moveUpVerification(boolean newLeftCell, boolean newRightCell, int currentState, int expectedState) {
        Blob blob = mock(Blob.class);
        when(blob.get(4,3)).thenReturn(newLeftCell);
        when(blob.get(5,3)).thenReturn(newRightCell);

        assertEquals(expectedState, Walker.moveUp(blob, Tuple.of(5, 5), currentState));

        verify(blob).get(4,3);
        verify(blob).get(5,3);
        verifyNoMoreInteractions(blob);
    }

    private void moveDownVerification(boolean newLeftCell, boolean newRightCell, int currentState, int expectedState) {
        Blob blob = mock(Blob.class);
        when(blob.get(4,6)).thenReturn(newLeftCell);
        when(blob.get(5,6)).thenReturn(newRightCell);

        assertEquals(expectedState, Walker.moveDown(blob, Tuple.of(5, 5), currentState));

        verify(blob).get(4,6);
        verify(blob).get(5,6);
        verifyNoMoreInteractions(blob);
    }

    private void moveLeftVerification(boolean newTopCell, boolean newBottomCell, int currentState, int expectedState) {
        Blob blob = mock(Blob.class);
        when(blob.get(3,4)).thenReturn(newTopCell);
        when(blob.get(3,5)).thenReturn(newBottomCell);

        assertEquals(expectedState, Walker.moveLeft(blob, Tuple.of(5, 5), currentState));

        verify(blob).get(3,4);
        verify(blob).get(3,5);
        verifyNoMoreInteractions(blob);
    }

    private void moveRightVerification(boolean newTopCell, boolean newBottomCell, int currentState, int expectedState) {
        Blob blob = mock(Blob.class);
        when(blob.get(6,4)).thenReturn(newTopCell);
        when(blob.get(6,5)).thenReturn(newBottomCell);

        assertEquals(expectedState, Walker.moveRight(blob, Tuple.of(5, 5), currentState));

        verify(blob).get(6,4);
        verify(blob).get(6,5);
        verifyNoMoreInteractions(blob);
    }

}