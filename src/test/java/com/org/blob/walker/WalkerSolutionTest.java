package com.org.blob.walker;

import com.org.blob.Solution;
import com.org.blob.Tuple;
import org.junit.Test;

import static com.org.blob.walker.Walker.Move.*;
import static org.junit.Assert.assertEquals;

public class WalkerSolutionTest {

    @Test
    public void testWalker() throws Exception {
        WalkerSolution solution = new WalkerSolution(Tuple.of(5, 5));
        assertEquals(new Solution(-1, 5, 5, 5, 5), solution.toSolution());

        solution.handleMove(RIGHT);
        assertEquals(new Solution(-1, 5, 5, 5, 5), solution.toSolution());

        solution.handleMove(LEFT);
        assertEquals(new Solution(-1, 5, 5, 5, 5), solution.toSolution());

        solution.handleMove(LEFT);
        assertEquals(new Solution(-1, 5, 4, 5, 5), solution.toSolution());

        solution.handleMove(LEFT);
        assertEquals(new Solution(-1, 5, 3, 5, 5), solution.toSolution());

        solution.handleMove(RIGHT);
        assertEquals(new Solution(-1, 5, 3, 5, 5), solution.toSolution());

        solution.handleMove(RIGHT);
        assertEquals(new Solution(-1, 5, 3, 5, 5), solution.toSolution());

        solution.handleMove(RIGHT);
        assertEquals(new Solution(-1, 5, 3, 5, 5), solution.toSolution());

        solution.handleMove(RIGHT);
        assertEquals(new Solution(-1, 5, 3, 5, 6), solution.toSolution());

        solution.handleMove(DOWN);
        assertEquals(new Solution(-1, 5, 3, 5, 6), solution.toSolution());

        solution.handleMove(DOWN);
        assertEquals(new Solution(-1, 5, 3, 6, 6), solution.toSolution());

        solution.handleMove(UP);
        assertEquals(new Solution(-1, 5, 3, 6, 6), solution.toSolution());

        solution.handleMove(UP);
        assertEquals(new Solution(-1, 5, 3, 6, 6), solution.toSolution());

        solution.handleMove(UP);
        assertEquals(new Solution(-1, 4, 3, 6, 6), solution.toSolution());

        solution.handleMove(DOWN);
        assertEquals(new Solution(-1, 4, 3, 6, 6), solution.toSolution());

        solution.handleMove(DOWN);
        assertEquals(new Solution(-1, 4, 3, 6, 6), solution.toSolution());

        solution.handleMove(DOWN);
        assertEquals(new Solution(-1, 4, 3, 6, 6), solution.toSolution());

        solution.handleMove(DOWN);
        assertEquals(new Solution(-1, 4, 3, 7, 6), solution.toSolution());

        solution.handleMove(UP);
        assertEquals(new Solution(-1, 4, 3, 7, 6), solution.toSolution());

        solution.handleMove(UP);
        assertEquals(new Solution(-1, 4, 3, 7, 6), solution.toSolution());

        solution.handleMove(UP);
        assertEquals(new Solution(-1, 4, 3, 7, 6), solution.toSolution());

        solution.handleMove(UP);
        assertEquals(new Solution(-1, 4, 3, 7, 6), solution.toSolution());

        solution.handleMove(UP);
        assertEquals(new Solution(-1, 3, 3, 7, 6), solution.toSolution());
    }
}