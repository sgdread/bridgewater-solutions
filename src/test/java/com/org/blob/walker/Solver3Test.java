package com.org.blob.walker;

import com.org.blob.AbstractSolverTest;
import com.org.blob.Blob;
import com.org.blob.Solution;

public class Solver3Test extends AbstractSolverTest {

    @Override
    protected Solution solveUsingData(String[] data) {
        return ((WalkerSolution)Solver3.withBlob(Blob.fromData(data)).solve()).toSolution();
    }
}