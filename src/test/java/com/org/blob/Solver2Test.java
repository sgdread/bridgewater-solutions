package com.org.blob;

public class Solver2Test extends AbstractSolverTest {

    @Override
    protected Solution solveUsingData(String[] data) {
        return Solver2.withBlob(Blob.fromData(data)).solve();
    }
}