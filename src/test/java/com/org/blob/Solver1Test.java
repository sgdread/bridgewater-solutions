package com.org.blob;

public class Solver1Test extends AbstractSolverTest{

    @Override
    protected Solution solveUsingData(String[] data) {
        return Solver1.withBlob(Blob.fromData(data)).solve();
    }

}