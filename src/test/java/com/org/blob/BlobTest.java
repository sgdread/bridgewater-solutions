package com.org.blob;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class BlobTest {

    private String[] data;
    private Blob blob;

    @Before
    public void setUp() throws Exception {
        // @formatter:off
        data = new String[] {
                 "0000000000",
                 "0011100000",
                 "0011111000",
                 "0010001000",
                 "0011111000",
                 "0000101000",
                 "0000101000",
                 "0000111000",
                 "0000000000",
                 "0000000000"
         };
        // @formatter:on
        blob = Blob.fromData(data);
    }

    @Test
    public void blob_should_be_properly_constructed_from_string_array() throws Exception {
        String expectedString = Arrays.stream(data).reduce("", (a, b) -> a + (a.length() > 0 ? "\n" + b : b));
        assertEquals(expectedString, blob.toString());
    }

    @Test
    public void get_should_return_proper_values() {
        assertEquals(0, blob.getReads());
        assertEquals(false, blob.get(0,0));
        assertEquals(true, blob.get(2,1));
        assertEquals(false, blob.get(5,5));
        assertEquals(false, blob.get(5,6));
        assertEquals(true, blob.get(5,7));
        assertEquals(5, blob.getReads());
    }
}